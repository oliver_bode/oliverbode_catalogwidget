# Magento2 Catalog Widget #

Magento2 Catalog Widget

## Features ##
* Well organized, easy to use and professionally designed.
* Uses the Magento 2 core Cms widget/blocks/pages functionality. 
* Extends the default Catalog Widget by putting it in an owl slider

## Install Magento 2 Catalog Widget ##
### Manual Installation ###

Install Catalog Widget

* Download the extension
* Unzip the file
* Create a folder {Magento root}/app/code/Oliverbode/CatalogWidget
* Copy the content from the unzip folder
* Optionally remove Setup folder if you don't want to install the demo

### Composer Installation ###

```
#!

composer config repositories.oliverbode_catalogwidget vcs https://oliver_bode@bitbucket.org/oliver_bode/oliverbode_catalogwidget.git
composer require oliverbode/catalogwidget
```

## Enable Catalog Widget ##

```
#!

php -f bin/magento module:enable --clear-static-content Oliverbode_CatalogWidget
php -f bin/magento setup:upgrade
php -f bin/magento setup:static-content:deploy
```

## Creating a Catalog Product Slider ##

Goto Content -> Widget and create a new widget. Choose the "Catalog Product List"

Select the pages where you wish to insert the widget and the block where you would like it to display. In the "Widget Options" fill in the widget options and save widget.


### Removing Catalog Widget ###

The demo can be viewed at: {Magento2 Base Url}/product-slider/

To remove it: log into your Magetno Admin, then goto Content -> Pages

Delete the "Catalog Widget".